import { useContext, useEffect, useState } from 'react'
import AuthContext from '../stores/authContext'
import styles from '../styles/Guides.module.css'
// import guides from '../functions/guides'

export default function Guides() {
  const {user, authReady} = useContext(AuthContext);
  const [guides,setGuides]=useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    if(authReady)
    {
    fetch('/.netlify/functions/guides', user && {
        headers: {
          Authorization: 'Bearer ' + user.token.access_token}
    })
    .then(res => {
      if(!res.ok)
      {
        throw Error('You must be logged to see the Content');
      }
      return res.json()
    })
    .then(data => {
      setGuides(data)
      setError(null)
    })
    .catch( (err) => {
      setError(err.message)
      setGuides(null)
    })
    }
  },[user,authReady])

  return (
    <div className={styles.guides}>
       {!authReady && <div>
            Loading.....
       </div>}
       {error &&<div className={styles.error}><p>{error}</p> </div>}
       {guides && <div>
        {guides.map((guide) => (
          <div className={styles.card} key={guide.title}>
            <h3>{guide.title}</h3>
            <h5>Written by : {guide.author }</h5>
            <p>Officiis deleniti rem aspernatur odit hic autem neque repellat alias? Debitis veniam inventore ipsum similique quos animi ipsa asperiores fuga dolor id. Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis deleniti rem aspernatur odit hic autem neque repellat alias? Debitis veniam inventore ipsum similique quos animi ipsa asperiores fuga dolor id.</p>
          </div>
        ) )}
        </div>}
    </div> 
  )
}